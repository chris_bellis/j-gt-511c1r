/*
 * Chris Bellis
 * 2014
 */
package gt511c1r;

import gt511c1r.natcode.NativeFingerprint;
import java.awt.image.BufferedImage;

/**
 *
 * @author Chris
 */
public class Fingerprint 
{
    private final NativeFingerprint fp;
    
    // TODO: Throw Exceptions for NACK conditions
    
    public Fingerprint()
    {
        fp = new NativeFingerprint();
        fp.open();
    }
    public void close()
    {
        fp.close();
    }
    public BufferedImage getImage()
    {
        fp.cmos_led(true);
        BufferedImage image = imageCapture();
        fp.cmos_led(false);
        return image;
    }
    
    private BufferedImage imageCapture()
    {
        waitForFingerPress();
        fp.capture(false);
        fp.get_image();
        try{Thread.sleep(200);}catch(Exception e){};
        byte[] data = fp.get_image_data();
        BufferedImage image = new BufferedImage(240, 216,BufferedImage.TYPE_BYTE_GRAY);
        image.getRaster().setDataElements(0,0,240,216,data);
        return image;
    }
    
    public BufferedImage getRawImage()
    {
        fp.cmos_led(true);
        fp.get_rawimage();
        try{Thread.sleep(200);}catch(Exception e){};
        byte[] data = fp.get_rawimage_data();
        BufferedImage image = new BufferedImage(240, 216,BufferedImage.TYPE_BYTE_GRAY);
        image.getRaster().setDataElements(0,0,240,216,data);
        fp.cmos_led(false);
        return image;
    }
    
    public boolean verify(int id)
    {
        fp.cmos_led(true);
        waitForFingerPress();
        fp.verify(id);
        fp.cmos_led(false);
        return fp.get_last_ack() == NativeFingerprint.ACK;
    }
    
    public int identify()
    {
        fp.cmos_led(true);
        waitForFingerPress();
        fp.identify();
        int id = fp.get_last_ack_param();
        fp.cmos_led(false);
        return id;
    }
    
    public void waitForFingerPress()
    {
        boolean fingerPressed = false;
        do{
            fp.is_press_finger();
            int status = fp.get_last_ack_param();
            if(status == 0) fingerPressed = true;
            if(status != 0) fingerPressed = false;
        }while(!fingerPressed);
    }
    
    public void enroll(int pos, int numTime)
    {
        fp.cmos_led(true);
        waitForFingerPress();
        do{
            waitForFingerPress();
            fp.enroll_nth(pos, numTime);
        }while(fp.get_last_ack() == NativeFingerprint.NACK);
        fp.cmos_led(false);
    }
    
    public void fullEnroll(int pos)
    {
        for(int i = 1;i<=3;i++)
        {
            enroll(pos,i);
            try{Thread.sleep(1000);}catch(Exception e){}
        }
    }
    
    public void deleteDatabase()
    {
        fp.delete_all();
    }
    
    public void deleteEnrolled(int pos)
    {
        fp.delete_id(pos);
    }
    
    public int getEnrollCount()
    {
        fp.enroll_count();
        return fp.get_last_ack_param();
    }
    
    public void ledConrol(boolean ctrl)
    {
        fp.cmos_led(ctrl);
    }
}
