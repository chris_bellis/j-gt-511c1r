/*
 * Chris Bellis
 * 2014
 */
package gt511c1r.natcode;

/**
 *
 * @author Chris
 */
public class NativeFingerprint {
    static{
        System.loadLibrary("lib/NativeFingerprint");
    }
    public final static int ACK = 0x30;
    public final static int NACK = 0x31;
    public native boolean open();
    public native int close();
    public native int internal_check();
    public native int change_baudrate(int newBaud);
    public native int cmos_led(boolean bOn);
    public native int enroll_count();
    public native int check_enrolled(int nPos);
    public native int enroll_start(int nPos);
    public native int enroll_nth(int nPos, int nTurn);
    public native int is_press_finger();
    public native int delete_id(int nPos);
    public native int delete_all();
    public native int verify(int nPos);
    public native int identify();
    public native int verify_template(int nPos);
    public native int identify_template(int nPos);
    public native int capture(boolean bBest);
    public native int get_image();
    public native byte[] get_image_data();
    public native int get_rawimage();
    public native byte[] get_rawimage_data();
    public native int get_template(int nPos);
    public native int add_template(int nPos);
    public native void set_template_data(byte[] data);
    public native int get_database_start();
    public native int get_database_end();
    public native int get_last_ack();
    public native int get_last_ack_param();
}
