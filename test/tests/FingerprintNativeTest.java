/*
 * Chris Bellis
 * 2014
 */
package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import gt511c1r.natcode.NativeFingerprint;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Chris
 */
public class FingerprintNativeTest {
    
    public FingerprintNativeTest() {
    }
    
    NativeFingerprint fp;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() 
    {
        fp = new NativeFingerprint();
        assertTrue(fp.open());
    }
    
    @After
    public void tearDown()
    {
        fp.cmos_led(false);
        fp.close();
        fp = null;
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testLed()
    {
        System.out.print("Testing LED...");
        fp.cmos_led(true);
        try{Thread.sleep(1000);}catch(Exception e){}
        fp.cmos_led(false);
        assertEquals(NativeFingerprint.ACK, fp.get_last_ack());
        System.out.println("Done!");
    }
    
    @Test
    public void testEnrollCount()
    {
        System.out.print("Testing Enroll Counter...");
        fp.delete_all();
        fp.enroll_count();
        int numEnrolled = fp.get_last_ack_param();
        assertEquals(numEnrolled, 0);
        System.out.println("Done!");
    }
    
    @Test
    public void testEnrollment()
    {
        fp.delete_all();
        fp.cmos_led(true);
        assertEquals(fp.get_last_ack(), NativeFingerprint.ACK);
        int id = 5;
        fp.enroll_start(id);
        assertEquals(fp.get_last_ack(), NativeFingerprint.ACK);
        for(int i = 1; i<=3; i++)
        {
            System.out.println("Press Finger");
            boolean fingerPressed = false;

            do{
                do{
                    fp.is_press_finger();
                    int status = fp.get_last_ack_param();
                    if(status == 0) fingerPressed = true;
                    if(status != 0) fingerPressed = false;
                }while(!fingerPressed);
                
                fp.enroll_nth(id, i);
                
            }while(fp.get_last_ack() == NativeFingerprint.NACK);
            System.out.println("Release Finger");
            fingerPressed = true;
            do{
                fp.is_press_finger();
                int status = fp.get_last_ack_param();
                if(status != 0) fingerPressed = false;
                if(status == 0) fingerPressed = true;
            }while(fingerPressed);
            System.out.println("Enroll#" + i);
        }
        
        System.out.println("Press Finger");
        boolean fingerPressed = false;
        do{
            fp.is_press_finger();
            int status = fp.get_last_ack_param();
            if(status == 0) fingerPressed = true;
        }while(!fingerPressed);
        fp.identify();
        int param = fp.get_last_ack_param();
        assertEquals(param, id);
        
        fingerPressed = false;
        do{
            fp.is_press_finger();
            int status = fp.get_last_ack_param();
            if(status == 0) fingerPressed = true;
        }while(!fingerPressed);
        
        fp.verify(id);
        assertEquals(NativeFingerprint.ACK,fp.get_last_ack());
        
        System.out.println("Release Finger");
        fp.enroll_count();
        int enrollCount = fp.get_last_ack_param();
        assertEquals(1,enrollCount);
    }
    
    @Test
    public void TestCapture() throws IOException
    {
        fp.cmos_led(true);
        System.out.println("Press Finger");
        boolean fingerPressed = false;
        do{
            fp.is_press_finger();
            int status = fp.get_last_ack_param();
            if(status == 0) fingerPressed = true;
            if(status != 0) fingerPressed = false;
        }while(!fingerPressed);
        fp.capture(false);
        fp.get_image();
        try{Thread.sleep(200);}catch(Exception e){};
        byte[] data = fp.get_image_data();
        BufferedImage image = new BufferedImage(240, 216,BufferedImage.TYPE_BYTE_GRAY);
        image.getRaster().setDataElements(0,0,240,216,data);
        ImageIcon icon=new ImageIcon(image);
        JFrame frame=new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(200,300);
        JLabel lbl=new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try{Thread.sleep(5000);}catch(Exception e){};
    }
}
