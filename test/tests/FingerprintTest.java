/*
 * Chris Bellis
 * 2014
 */
package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import gt511c1r.Fingerprint;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author Chris
 */
public class FingerprintTest {
    
    public FingerprintTest() {
    }
    
    private Fingerprint fp;
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        fp = new Fingerprint();
    }
    
    @After
    public void tearDown() {
        fp.close();
        fp = null;
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void TestImageCapture()
    {
        BufferedImage image = fp.getRawImage();
        displayImage(image);
    }
    
    @Test
    public void TestImageCaptureWithWait()
    {
        System.out.println("Press Finger");
        BufferedImage image = fp.getImage();
        displayImage(image);
    }
    
    @Test
    public void TestEnrollandIdentify()
    {
        System.out.println("Press Finger When Lit");
        fp.deleteDatabase();
        fp.fullEnroll(5);
        assertEquals(1, fp.getEnrollCount());
        
        assertEquals(5, fp.identify());
        assertTrue(fp.verify(5));
    }
    
    public void displayImage(BufferedImage image)
    {
        ImageIcon icon=new ImageIcon(image);
        JFrame frame=new JFrame();
        frame.setLayout(new FlowLayout());
        frame.setSize(200,300);
        JLabel lbl=new JLabel();
        lbl.setIcon(icon);
        frame.add(lbl);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        try{Thread.sleep(1000);}catch(Exception e){};
    }
}
